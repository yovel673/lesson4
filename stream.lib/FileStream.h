#pragma once
#include "OutStream.h"
namespace msl {

	class FileStream : public OutStream
	{
	public:
		FileStream(char * nameOfFile);
		~FileStream();
	};

}
