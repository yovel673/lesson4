#include "OutStream.h"
#include <stdio.h>
using namespace msl;

OutStream::OutStream()
{
	_kovetz = stdout; //so the class will keep behave like before.
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	if (_kovetz)
		fprintf(_kovetz, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	if (_kovetz)
		fprintf(_kovetz, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE * kovetz))
{
	if (_kovetz)
		pf(_kovetz);
	return *this;
}


void endline(FILE * kovetz)
{
	fprintf(kovetz, "\n");
}
