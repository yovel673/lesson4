#include "Logger.h"
#include <stdio.h>
#include <string.h>
Logger::Logger()
{
	os = OutStream(); //building os
	setStartLine(); //setting startline first time
}
Logger::~Logger()
{
	
}

/*
The function is incresing the log line num every time it calld and prints LOG
*/
void Logger::setStartLine()
{
	static unsigned int num = 0;
	num++;
	os << endline << "LOG " << num << ":  ";
}

/*the function checks is startnewline is true and according to that calling the function*/
Logger& operator<<(Logger& l, const char *msg)
{
	if (l._startLine == true)
	{
		l.setStartLine();
	}
	l.os << msg;
	
	
	return l;
}
Logger& operator<<(Logger& l, int num)
{
	if (l._startLine == true)
	{
		l.setStartLine();
	}
	l.os << num;

	return l;
}

Logger& operator<<(Logger& l, void(*pf)(FILE * kovetz))
{
	if (pf == endline)
	{
		l._startLine = true;
	}
	else
	{
		if (l._startLine)
			l.setStartLine();
		l.os << pf;
	}
	
	return l;
}