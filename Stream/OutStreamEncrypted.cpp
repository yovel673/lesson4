#include "OutStreamEncrypted.h"
#include <string.h>


/*c'tor*/
OutStreamEncrypted::OutStreamEncrypted(int offset)
{
	_offset = offset;
}

/*d'tor*/
OutStreamEncrypted::~OutStreamEncrypted()
{
}
/*
The operator encryprts strings by the offset
*/
OutStream& OutStreamEncrypted::operator<<(const char *str) //decrypting string
{
	int len = strlen(str);
	int i = 0;
	char toPrint = 'a';
	for (i = 0; i < len; i++)
	{
		toPrint = str[i];
		if (str[i] <= 126 && str[i] >= 32)
		{
			toPrint = toPrint + _offset;
			if (toPrint > 126)
				toPrint = toPrint % 126 + 32;
			printf("%c", toPrint);
			
		}
		else
		{
			printf("%c", str[i]);
		}
	}
	return *this;
}
/*encrypting ints*/
OutStream& OutStreamEncrypted::operator<<(int num)
{
	int digit = num;


	while (digit != 0)
	{
		printf("%d", (digit % 10) + _offset);

		digit /= 10;
	}
	return *this;
}

