#pragma once
#include "OutStream.h"

class OutStreamEncrypted: public OutStream
{
public:
	OutStreamEncrypted(int offset);
	~OutStreamEncrypted();

	OutStream& OutStreamEncrypted::operator<<(const char *str);
	OutStream& OutStreamEncrypted::operator<<(int num);

	

private:
	int _offset;
};

