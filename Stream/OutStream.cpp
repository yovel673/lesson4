#include "OutStream.h"
#include <stdio.h>


OutStream::OutStream()
{
	_kovetz = stdout; //so the class will keep behave like before.
}

OutStream::~OutStream()
{
}
/*wrinting / printing strings*/
OutStream& OutStream::operator<<(const char *str)
{
	if(_kovetz)
		fprintf(_kovetz,"%s", str);
	return *this;
}
/*writing printing ints*/
OutStream& OutStream::operator<<(int num)
{
	if(_kovetz)
		fprintf(_kovetz,"%d", num);
	return *this;
}
/*
calling new line
*/
OutStream& OutStream::operator<<(void(*pf)(FILE * kovetz))
{
	if(_kovetz)
		pf(_kovetz);
	return *this;
}


void endline(FILE * kovetz)
{
	fprintf(kovetz, "\n");
}
